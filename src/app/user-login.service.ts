import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Country } from './country';
import { States } from './states';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UserLoginService {
  private _url:string='assets/data/country.json';
  private _url2:string='assets/data/states.json';
  constructor(private http:HttpClient) { }

  public getUser(user:User)
  {
   return this.http.post("http://localhost:8080/login",user);

  }
  public getStates()
  {
    return this.http.get<States[]>(this._url2);
  }
  public getCountry()
  {
    return this.http.get<Country[]>(this._url);
  }
  public registerUser(user:User)
  {
    return this.http.post<string>("http://localhost:8080/register",user);
  }
}
