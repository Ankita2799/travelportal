export class User
{
        public username!:string;
        public firstname!:string;
        public lastname!:string;
        public businessunit!:string;
        public title!:string;
        public email!:string;
        public password!:string;
        public telephone!:number;
        public address1!:string;
        public address2!:string;
        public city!:string;
        public state!:string;
        public zip!:number;
        public country!:string
}