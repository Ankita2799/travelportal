import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../user';
import { UserLoginService } from '../user-login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: User=new User();
  constructor(private httpService:UserLoginService,
    private router:Router,
    private aroute:ActivatedRoute) { }

  ngOnInit(): void {
  }
  login()
  {
    this.httpService.getUser(this.user).subscribe(data=>{
      if(data == null)
      {
        console.log("Invalid username or password");
      }
      else{
        this.router.navigate(['/home']);
      }
    })
  }

}
