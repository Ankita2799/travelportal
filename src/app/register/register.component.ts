import { Component, OnInit } from '@angular/core';
import { Country } from '../country';
import { States } from '../states';
import { User } from '../user';
import { UserLoginService } from '../user-login.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  states: States[]=[];
  country:Country[]=[];
  user:User=new User();
  message!:string;
  constructor(private httpService:UserLoginService) { }

  ngOnInit(): void {
    this.httpService.getStates().subscribe(data=>
    {
      this.states=data; 
    });
    this.httpService.getCountry().subscribe(response=>
      {
        this.country=response;
      })
  }

  registerUser()
  {
    this.httpService.registerUser(this.user).subscribe(data=>
      {
       this.message=data; 
      });
  }
}
